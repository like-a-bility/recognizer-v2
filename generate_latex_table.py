import json
import os


def generate_latex():
    json_file = "/home/eliander/Scrivania/test_images/json_out.json"
    latex_file = "/home/eliander/Scrivania/test_images/generateTable.tex"

    latex = open(latex_file, "w")
    toWrite = " \\documentclass{article} \
                \\usepackage[utf8]{inputenc} \
                \\usepackage[english]{babel} \
                \\usepackage{blindtext} \
                \\usepackage{multirow} \
                \\usepackage{graphicx} \
                \\usepackage{longtable} \
                \\usepackage{multicol} \
                \\usepackage[margin=0.2in]{geometry} \
                \\begin{document} \
                \\begin{longtable}{p{4cm}|p{4cm}|p{6cm}|p{2cm}} \
                    \\hline \
                    Immagine & Hypothesis & References & BLEU - 4 \\\\ [0.5ex] \
                    \\hline"

    with open(json_file) as json_file:
        data = json.load(json_file)
        for filename in data:
            if filename=="total_bleu":
                toWrite += "Total Bleu: & " + str(data[filename])[:10] +" & & \\end{longtable} \\end{document}";
                break
            filex = str(filename)
            hypotesys = str(data[filename]["hypotesys"])
            references = data[filename]["references"]
            bleu_score = str(data[filename]["bleu"])[:10]
            toWrite = toWrite + "\\raisebox{-1\\height}{\\includegraphics[width=4cm]{" + filex + "}} & "
            toWrite += hypotesys + " & "
            toWrite += "\\begin{itemize}"
            for ref in references:
                toWrite += " \\item " + ref
            toWrite += " \\end{itemize} & "
            toWrite += str(bleu_score)
            toWrite += " \\\ \\hline "

    latex.write(toWrite)

