import argparse
import json
import time
from multiprocessing import Queue, Process, Lock
from urllib.error import HTTPError
from urllib.request import urlopen

import imageio
import numpy as np
import torch
import torch.nn.functional as F
import torchvision.transforms as transforms
from scipy.misc import imresize

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

transform = transforms.Compose([transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])


def caption_image_beam_search(encoder, decoder, image, word_map, beam_size=3):
    """
    Reads an image and captions it with beam search.

    :param encoder: encoder model
    :param decoder: decoder model
    :param image_path: path to image
    :param word_map: word map
    :param beam_size: number of sequences to consider at each decode-step
    :return: caption, weights for visualization
    """

    k = beam_size
    vocab_size = len(word_map)

    # Encode
    image = image.unsqueeze(0)  # (1, 3, 256, 256)
    encoder_out = encoder(image)  # (1, enc_image_size, enc_image_size, encoder_dim)
    encoder_dim = encoder_out.size(3)

    # Flatten encoding
    encoder_out = encoder_out.view(1, -1, encoder_dim)  # (1, num_pixels, encoder_dim)
    num_pixels = encoder_out.size(1)

    # We'll treat the problem as having a batch size of k
    encoder_out = encoder_out.expand(k, num_pixels, encoder_dim)  # (k, num_pixels, encoder_dim)

    # Tensor to store top k previous words at each step; now they're just <start>
    k_prev_words = torch.LongTensor([[word_map['<start>']]] * k).to(device)  # (k, 1)

    # Tensor to store top k sequences; now they're just <start>
    seqs = k_prev_words  # (k, 1)

    # Tensor to store top k sequences' scores; now they're just 0
    top_k_scores = torch.zeros(k, 1).to(device)  # (k, 1)

    # Lists to store completed sequences, their alphas and scores
    complete_seqs = list()
    complete_seqs_scores = list()

    # Start decoding
    step = 1
    h, c = decoder.init_hidden_state(encoder_out)

    # s is a number less than or equal to k, because sequences are removed from this process once they hit <end>
    while True:

        embeddings = decoder.embedding(k_prev_words).squeeze(1)  # (s, embed_dim)

        awe, alpha = decoder.attention(encoder_out, h)  # (s, encoder_dim), (s, num_pixels)

        gate = decoder.sigmoid(decoder.f_beta(h))  # gating scalar, (s, encoder_dim)
        awe = gate * awe

        h, c = decoder.decode_step(torch.cat([embeddings, awe], dim=1), (h, c))  # (s, decoder_dim)

        scores = decoder.fc(h)  # (s, vocab_size)
        scores = F.log_softmax(scores, dim=1)
        # Add
        scores = top_k_scores.expand_as(scores) + scores  # (s, vocab_size)

        # For the first step, all k points will have the same scores (since same k previous words, h, c)
        if step == 1:
            top_k_scores, top_k_words = scores[0].topk(k, 0, True, True)  # (s)
        else:
            # Unroll and find top scores, and their unrolled indices
            top_k_scores, top_k_words = scores.view(-1).topk(k, 0, True, True)  # (s)

        # Convert unrolled indices to actual indices of scores
        prev_word_inds = top_k_words / vocab_size  # (s)
        next_word_inds = top_k_words % vocab_size  # (s)

        # Add new words to sequences, alphas
        seqs = torch.cat([seqs[prev_word_inds], next_word_inds.unsqueeze(1)], dim=1)  # (s, step+1)

        # Which sequences are incomplete (didn't reach <end>)?
        incomplete_inds = [ind for ind, next_word in enumerate(next_word_inds) if
                           next_word != word_map['<end>']]
        complete_inds = list(set(range(len(next_word_inds))) - set(incomplete_inds))

        # Set aside complete sequences
        if len(complete_inds) > 0:
            complete_seqs.extend(seqs[complete_inds].tolist())
            complete_seqs_scores.extend(top_k_scores[complete_inds])
        k -= len(complete_inds)  # reduce beam length accordingly

        # Proceed with incomplete sequences
        if k == 0:
            break
        seqs = seqs[incomplete_inds]
        h = h[prev_word_inds[incomplete_inds]]
        c = c[prev_word_inds[incomplete_inds]]
        encoder_out = encoder_out[prev_word_inds[incomplete_inds]]
        top_k_scores = top_k_scores[incomplete_inds].unsqueeze(1)
        k_prev_words = next_word_inds[incomplete_inds].unsqueeze(1)

        # Break if things have been going on too long
        if step > 50:
            break
        step += 1

    i = complete_seqs_scores.index(max(complete_seqs_scores))

    return complete_seqs[i]


def captionize(img):
    # Read image and process
    try:
        # Encode, decode with attention and beam search
        seq = caption_image_beam_search(encoder, decoder, img, word_map, args.beam_size)

        # generate the caption
        caption = ' '.join([rev_word_map[ind] for ind in seq])
        return caption
    except RuntimeError as e:
        return None


def get_caption_json():
    try:
        with urlopen('http://likeability.brighenti.me/api/caption') as request:
            contents = request.read()
            json_caption = json.loads(contents)
            return json_caption, request.getcode()
    except HTTPError as e:
        return None, e.getcode()


def save_caption(json_caption, caption, status):
    to_send = {
        'id': json_caption['id'],
        'caption': caption,
        'status': status
    }
    return urlopen('http://likeability.brighenti.me/api/caption/save', data=bytes(json.dumps(to_send), encoding='utf8')).read()


def process_prepare_image(q_in, lock):
    with lock:
        results, code = get_caption_json()
    while code == 200 and len(results) > 0:
        for res in results:
            try:
                print('[%s] Preparing image' % (str(res['id'])))
                with urlopen(res['url']) as file:
                    img = imageio.imread(file.read(), as_gray=False, pilmode='RGB')
                    if len(img.shape) == 2:
                        img = img[:, :, np.newaxis]
                        img = np.concatenate([img, img, img], axis=2)
                    img = img[:, :, :3]  # remove alpha channel
                    img = imresize(img, (256, 256)).transpose(2, 0, 1) / 255.
                    img = torch.FloatTensor(img)

                    q_in.put([transform(img), res])
            except HTTPError:
                print('[%s] Error while preparing image' % (str(res['id'])))
                q_in.put([None, res])
        with lock:
            results, code = get_caption_json()
    q_in.put([None, None])


def process_caption_image(q_in, q_out):
    while True:
        img, res = q_in.get()
        if img is None and res is None:
            break

        if img is not None:
            status = 'OK'
            cap = captionize(img.to(device))
            print('[%s] Caption => %s' % (str(res['id']), cap))
        else:
            status = 'DELETE'
            cap = None

        q_out.put([cap, status, res])
    q_out.put([None, None])


def process_save_caption(q_out):
    while True:
        caption, status, res = q_out.get()
        if caption is None and res is None:
            break
        try:
            print('[%s] Saving => %s' % (str(res['id']), str(save_caption(res, caption, status))))
        except HTTPError:
            print('[%s] Error saving, caption is lost...' % (str(res['id'])))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Show, Attend, and Tell - Tutorial - Generate Caption')

    parser.add_argument('--model', '-m', default='BEST_checkpoint_coco_5_cap_per_img_5_min_word_freq.pth.tar',
                        help='path to model')
    parser.add_argument('--word_map', '-wm', default='WORDMAP_coco_5_cap_per_img_5_min_word_freq.json',
                        help='path to word map JSON')
    parser.add_argument('--beam_size', '-b', default=5, type=int, help='beam size for beam search')

    args = parser.parse_args()

    lock = Lock()
    queue_in = Queue(maxsize=20)
    queue_out = Queue(maxsize=20)

    fetchers = [Process(target=process_prepare_image, args=(queue_in, lock,)) for i in range(2)]

    savers = [Process(target=process_save_caption, args=(queue_out,)) for i in range(3)]

    for p in fetchers:
        p.start()
        time.sleep(2)

    for p in savers:
        p.start()

    timer = time.time()
    # Load model
    checkpoint = torch.load(args.model, map_location=device)
    decoder = checkpoint['decoder'].to(device)
    decoder.eval()
    encoder = checkpoint['encoder'].to(device)
    encoder.eval()
    print("--- models loaded in %s seconds ---" % (time.time() - timer))
    timer = time.time()

    # Load word map (word2ix)
    with open(args.word_map, 'r') as j:
        word_map = json.load(j)
    rev_word_map = {v: k for k, v in word_map.items()}  # ix2word

    print("--- dictionary loaded in %s seconds ---" % (time.time() - timer))
    timer = time.time()

    process_caption_image(queue_in, queue_out)

    for p in fetchers:
        p.join()

    for p in savers:
        p.join()
